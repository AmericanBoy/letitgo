/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Aydin Bagiyev
 */
public class FirstPage extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        try (PrintWriter out = response.getWriter()) {
            
            String login = request.getParameter("login");
            String pass = request.getParameter("pass");
            HttpSession session = request.getSession();
            String errorMessage="Please authorise";
            
            String realLogin= (String)session.getAttribute("nickname");
            String realPassword=(String)session.getAttribute("password");
        
            Cookie RealLogin = new Cookie("Nickname" ,realLogin);
            Cookie RealPass = new Cookie("Password", realPassword);
            Cookie Login = new Cookie("Login" ,login);
            Cookie Pass = new Cookie("Pass", pass);
            
                response.addCookie(RealLogin);
                response.addCookie(RealPass);
             
                response.addCookie(Login);
                response.addCookie(Pass);
       
          
            if(login !=null && pass != null) {
                if (!login.equals(realLogin)){
                errorMessage = "Login is wrong, Sir";
                
                }else {
                if(!pass.equals(realPassword)){
                    errorMessage = "Password is Wrong, Sir";
                }else
                {
                    session.setAttribute("authorised", "yes");
                    response.sendRedirect("SecondPage");
                }}
                    }
            
            
                    
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet FirstPage</title>");            
            out.println("</head>");
            out.println("<body style=\"background-color:#00868B; font-family:ar destine\">");
                
                out.println("<h4 style=\"color:red\">" + errorMessage + "</h4>");
            
                   
            
                 out.println("<form method=\"POST\">"
                      +   "Login: <input type=\"text\"  name =\"login\">"
                      +  "Password: <input type=\"password\" name =\"pass\">"
                      +   "<input style=\"background:#CD3700 ;border-style: ridge;border-radius: 50px;\" type=\"submit\" value=\"Sign in\">"
                     
                   
                      +   "</form>");
                   out.println("<form>"                   
                    + "<input style=\"background:#CD3700 ;border-style: ridge;border-radius: 50px;\" type=\"submit\" value=\"Registration\" name=\"register\" >"
                    + "</form>");
                   String register = request.getParameter("register");
                   if(register!=null && !register.equals("")){
                   response.sendRedirect("Register");}
                   Cookie[] cookies = request.getCookies();
            if (cookies!=null)
            {
                for (int i = 0; i < cookies.length; i++) {
                    out.println((i+1)+")"+cookies[i].getName());
                    out.println(cookies[i].getValue() + "<br>");
                }
            }
            out.println(request.getContextPath());      
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
