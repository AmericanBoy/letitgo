/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Aydin Bagiyev
 */
public class Register extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            String fullname = request.getParameter("fullname");
            String nickname = request.getParameter("nickname");
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            HttpSession session = request.getSession();
            String errormessage = "Welcome to registration Page";
            String registereds = (String)session.getAttribute("member");
            if(registereds != null && !registereds.equals("registered")){
                out.println("This data already exist Please input new one");
                
                
                
            }
           
            
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<h1>Registration Process</h1>");
            out.println("<title>Servlet Register</title>"); 
            out.println("<h2 style=\"color:Red\">"+errormessage+"</h2>");
            out.println("</head>");
            out.println("<body style=\"background-color:#00868B; font-family:ar destine\">");
            out.println("<form method=\"post\">"
                    + "Full name: <input type\"text\" name=\"fullname\">"
                    + "Nickname: <input type=\"text\" name=\"nickname\">"
                    + "Password: <input type=\"password\" name=\"password\">"
                    + "Email: <input type=\"mail\" name=\"email\">"
                    + "<input style=\"background:#CD3700 ;border-style: ridge;border-radius: 50px;\" align=\"center\" type=\"submit\" name=\"register\" value=\"Register\"></form>");
   
             if (nickname!=null && !nickname.equals("") && password!=null && !password.equals("")&& email!=null && 
                     !email.equals("")){
                session.setAttribute("nickname", nickname);
                session.setAttribute("password", password);
                response.sendRedirect("FirstPage");
                
             }
          
            
            out.println( request.getContextPath() );
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
